//
//  FNMapView.h
//  KMLEditor
//
//  Created by Francesco Novelli on 16/03/14.
//  Copyright (c) 2014 runcode. All rights reserved.
//

#import <MapKit/MapKit.h>
#import <CoreLocation/CoreLocation.h>
@interface FNMapView : MKMapView {
    CLLocationCoordinate2D firstPoint;
    BOOL clicked;
}

@property (nonatomic) BOOL mustDrawLine;

@end
