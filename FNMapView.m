//
//  FNMapView.m
//  KMLEditor
//
//  Created by Francesco Novelli on 16/03/14.
//  Copyright (c) 2014 runcode. All rights reserved.
//

#import "FNMapView.h"

@implementation FNMapView
- (void)mouseUp:(NSEvent *)theEvent {
    [super mouseUp:theEvent];
    if(self.mustDrawLine) {
        
        NSLog(@"MOUSE DOWN");
        //    CLLocationCoordinate2D coordinate = [self convertPoint:theEvent.locationInWindow toCoordinateFromView:self];
        NSPoint p0 = [self convertPoint:[theEvent locationInWindow] fromView:nil];	// initial point
        MKMapPoint pnt = [self _mapPointForPoint:p0];
        
        CLLocationCoordinate2D coordinate=MKCoordinateForMapPoint(pnt);
        
        NSLog(@"%f %f", coordinate.latitude, coordinate.longitude);
        
        
        if (clicked) {
            CLLocationCoordinate2D  points[2];
            
            points[0] = firstPoint;
            points[1] = coordinate;
            
            MKPolyline* poly = [MKPolyline polylineWithCoordinates:points count:2];
            poly.title = @"Colorado";
            
            [self addOverlay:poly level:MKOverlayLevelAboveRoads];
            
            
            clicked = NO;
        } else {
            firstPoint = coordinate;
            clicked = YES;
        }
    }
}

- (MKMapPoint) _mapPointForPoint:(NSPoint) pnt
{ // convert map point to bounds point (using visibleMapRect)
	NSRect bounds=[self bounds];
	MKMapRect visible=[self visibleMapRect];
	return MKMapPointMake(MKMapRectGetMinX(visible)+MKMapRectGetWidth(visible)*(pnt.x-NSMinX(bounds))/NSWidth(bounds),
						  MKMapRectGetMinY(visible)+MKMapRectGetHeight(visible)*(pnt.y-NSMinY(bounds))/NSHeight(bounds));
}
@end
