//
//  main.m
//  KMLEditor
//
//  Created by Francesco Novelli on 16/03/14.
//  Copyright (c) 2014 runcode. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, const char * argv[])
{
    return NSApplicationMain(argc, argv);
}
