//
//  AppDelegate.h
//  KMLEditor
//
//  Created by Francesco Novelli on 16/03/14.
//  Copyright (c) 2014 runcode. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "FNMapView.h"
@import MapKit;
@interface AppDelegate : NSObject <NSApplicationDelegate, MKMapViewDelegate>

@property (assign) IBOutlet NSWindow *window;
- (IBAction)drawLine:(id)sender;
@property (weak) IBOutlet FNMapView *mapView;
@property (weak) IBOutlet NSColorWell *colorWell;
- (IBAction)cleanMap:(id)sender;
- (IBAction)goBack:(id)sender;
@property (weak) IBOutlet NSSlider *slider;
- (IBAction)changeView:(id)sender;

@end
