//
//  AppDelegate.m
//  KMLEditor
//
//  Created by Francesco Novelli on 16/03/14.
//  Copyright (c) 2014 runcode. All rights reserved.
//

#import "AppDelegate.h"
#import "KMLParser.h"
#import "KML.h"
@implementation AppDelegate

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification
{
    // Insert code here to initialize your application
//    [_mapView setAcceptsTouchEvents:YES];

}

- (IBAction)drawLine:(id)sender {
    NSButton *button = (NSButton *)sender;
    self.mapView.mustDrawLine = button.state;
    
//    [_mapView removeOverlays:_mapView.overlays];
    
//    NSString *path = [[NSBundle mainBundle] pathForResource:@"difronte" ofType:@"kml"];
//    NSURL *url = [NSURL fileURLWithPath:path];
//    KMLParser *kmlParser = [[KMLParser alloc] initWithURL:url];
//    [kmlParser parseKML];
//    
//    // Add all of the MKOverlay objects parsed from the KML file to the map.
//    NSArray *overlays = [kmlParser overlays];
//    [_mapView addOverlays:overlays];
    
    CLLocationCoordinate2D  points[5];
    
    points[0] = CLLocationCoordinate2DMake(41.000512, -109.050116);
    points[1] = CLLocationCoordinate2DMake(41.002371, -102.052066);
    points[2] = CLLocationCoordinate2DMake(36.993076, -102.041981);
    points[3] = CLLocationCoordinate2DMake(36.99892, -109.045267);
    points[4] = CLLocationCoordinate2DMake(41.000512, -109.050116);
    MKPolyline* poly = [MKPolyline polylineWithCoordinates:points count:5];
    poly.title = @"Colorado";
    
    [_mapView addOverlay:poly];
    
    CLLocationCoordinate2D worldCoords[4] = { {90,-180}, {90,180}, {-90,180}, {-90,-180} };
    MKPolygon *worldOverlay = [MKPolygon polygonWithCoordinates:points
                                                          count:5];
    [self.mapView addOverlay:worldOverlay level:MKOverlayLevelAboveRoads];
//
//    // Add all of the MKAnnotation objects parsed from the KML file to the map.
//    NSArray *annotations = [kmlParser points];
//////    [_mapView addAnnotations:annotations];
//////    
//////    // Walk the list of overlays and annotations and create a MKMapRect that
//////    // bounds all of them and store it into flyTo.
//    MKMapRect flyTo = MKMapRectNull;
//    for (id <MKOverlay> overlay in overlays) {
//        if (MKMapRectIsNull(flyTo)) {
//            flyTo = [overlay boundingMapRect];
//        } else {
//            flyTo = MKMapRectUnion(flyTo, [overlay boundingMapRect]);
//        }
//    }
//    
//    for (id <MKAnnotation> annotation in annotations) {
//        MKMapPoint annotationPoint = MKMapPointForCoordinate(annotation.coordinate);
//        MKMapRect pointRect = MKMapRectMake(annotationPoint.x, annotationPoint.y, 0, 0);
//        if (MKMapRectIsNull(flyTo)) {
//            flyTo = pointRect;
//        } else {
//            flyTo = MKMapRectUnion(flyTo, pointRect);
//        }
//    }
//    
//    // Position the map so that all overlays and annotations are visible on screen.
//    _mapView.visibleMapRect = flyTo;

}

-(MKOverlayRenderer *)mapView:(MKMapView *)mapView rendererForOverlay:(id<MKOverlay>)overlay{
    if (![overlay isKindOfClass:[MKPolygon class]]) {
        
        MKPolylineRenderer *render = [[MKPolylineRenderer alloc] initWithOverlay:overlay];
        render.strokeColor = self.colorWell.color;
        render.lineWidth = self.slider.floatValue;
        return render;
    }
    MKPolygon *polygon = (MKPolygon *)overlay;
    MKPolygonRenderer *renderer = [[MKPolygonRenderer alloc] initWithPolygon:polygon];
    renderer.fillColor = [[NSColor darkGrayColor] colorWithAlphaComponent:0.4];
    renderer.strokeColor = self.colorWell.color;
    renderer.lineWidth = self.slider.floatValue;
    
    return renderer;
    
    
}

- (IBAction)cleanMap:(id)sender {
    [_mapView removeOverlays:_mapView.overlays];
}

- (IBAction)goBack:(id)sender {
    [_mapView removeOverlay:[_mapView.overlays lastObject]];
}
- (IBAction)changeView:(id)sender {
    
    NSSegmentedControl *segControl = (NSSegmentedControl *)sender;
    [_mapView setMapType:(MKMapType)segControl.selectedSegment];
}

- (NSString *)kmlFilePath
{
    NSDateFormatter *formatter = [NSDateFormatter new];
    formatter.timeStyle = NSDateFormatterFullStyle;
    formatter.dateFormat = @"yyyyMMddHHmmss";
    NSString *dateString = [formatter stringFromDate:[NSDate date]];
    
    NSString *fileName = [NSString stringWithFormat:@"log_%@.kml", dateString];
    return [NSTemporaryDirectory() stringByAppendingPathComponent:fileName];
}

- (NSString *)createKML
{
    // kml
    KMLRoot *kml = [KMLRoot new];
    
    // kml > document
    KMLDocument *document = [KMLDocument new];
    kml.feature = document;
    
//    NSArray *sortedTrackPoints = self.track.sotredTrackPoints;
//    
//    // kml > document > placemark#strat
//    TrackPoint *startPoint = sortedTrackPoints[0];
//    KMLPlacemark *startPlacemark = [self placemarkWithName:@"Start" coordinate:startPoint.coordinate];
//    [document addFeature:startPlacemark];
//    
//    // kml > document > placemark#line
//    KMLPlacemark *line = [self lineWithTrakPoints:sortedTrackPoints];
//    [document addFeature:line];
//    
//    // kml > document > placemark#end
//    TrackPoint *endPoint = [sortedTrackPoints lastObject];
//    KMLPlacemark *endPlacemark = [self placemarkWithName:@"End" coordinate:endPoint.coordinate];
//    [document addFeature:endPlacemark];
//
    
    
    KMLPolygon *poligon = [[KMLPolygon alloc] init];
    
    NSString *kmlString = kml.kml;
    
    // write kml to file
    NSError *error;
    NSString *filePath = [self kmlFilePath];
    if (![kmlString writeToFile:filePath atomically:YES encoding:NSUTF8StringEncoding error:&error]) {
        if (error) {
            NSLog(@"error, %@", error);
        }
        
        return nil;
    }
    
    return filePath;
}
@end
